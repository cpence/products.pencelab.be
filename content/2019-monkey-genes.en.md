---
title: "Research Ethics and Monkey Brains"
---

# Research Ethics and Monkey Brains

I was asked recently by the Brussels newspaper _La Libre_ to provide some
comments regarding research ethics concerning some recent experiments in
macaques, which involved inserting a gene responsible for human brain
development into a number of monkeys. (See [some very nice
coverage](https://www.technologyreview.com/s/613277/chinese-scientists-have-put-human-brain-genes-in-monkeysand-yes-they-may-be-smarter/)
at _MIT Technology Review._)

As always, with interviews – and especially in my second language! – things
didn't come off exactly the way that I might have liked, but I'm reasonably
happy with the end result. (In particular, I feel that the reporter was really
looking for the "anti-animal-experiment" line from the philosopher in the
debate, and so a few of my worries about experiments _on monkeys_ became worries
about experiments _on animals._) If you'd like to read the final, published
remarks, [you can find a PDF of the newspaper article
here.](http://archive.charlespence.net/media/2019-la-libre-singes.pdf)

## Does this experiment blur the border between human and animal?

Since the goal of the scientists involved is straightforwardly to produce
monkeys with a brain between that of the monkey and that of man, it's hard to
say anything but yes. But I think that, even though it's important to recognize
the particular capacities of humans, it's not necessary to believe that the
monkeys produced in this way are somehow "human" to be completely horrified by
this experiment.

For example, one of the authors of the new study has claimed that, because the
subjects are only apes (as opposed to great apes), there's a sufficient
difference to support the idea that the research poses no ethical problems. I
don't know any data that could justify such a dramatic gap, ethically speaking,
between two clades so evolutionarily close. On the contrary, we learn little by
little that, in monkeys and apes (and also in cephalopods, at least), there's a
rich inner life, and they are almost certainly capable of joy, suffering, and
pain. As far as I'm concerned, that's sufficient to establish ethical problems,
even if there's no worry here of a "planet of the apes."

## Do you see concerns in this experiment?

I see two levels of concerns in this regard. First, there's ethical problems
linked to any experiment using monkeys as subjects. It's probable that in
certain domains (e.g., the transmission of HIV) that the use of monkeys as
animal models presents real advantages. But we always must evaluate whether
there is really no alternative, and if the knowledge acquired is worth the harm
to the animals.

This experiment thus aims to add a human gene to monkeys which controls, at
least in part, the development of the brain. (They plan to add more genes in the
coming months.) The scientific question that this treats is one of great
importance: what are the steps in the evolution of the human brain? Why are we
so distinctive? It's a subject absolutely central to human evolution.

But how can this experiment really consider it? We have some monkeys (macaques,
more precisely), that have diverged from our last common ancestor around 23
million years ago. Evolutionarily they haven't been static – they've absolutely
changed quite a bit during this period of time, and the same for humans (and our
other common ancestors, like gorillas, chimpanzees, etc., who have all been
evolving at the same time). Then, after 23 million years of evolution, we are
going to add a single human gene to a monkey, and we find (maybe – it's
important to say that this study couldn't be published in major international
journals) that they become a bit more accomplished at a short-term memory
test. (Half of them die.)

Thus, it's not at all clear, even in the absence of ethical worries, if the
experiment sheds any light on human evolution. Given the other large ethical
problems, it's unimaginable that the cost-benefit analysis could, in the end, be
positive.

## What is the basis for human and animal dignity? Is it only the cognitive capacity of these species?

I've already spoken of the emotional capacity, of the rich internal life of
animals. One doesn't have to be a genuine utilitarian to share the opinion of
the founder of the movement, Jeremy Bentham, who wrote: "The question is not,
Can they reason? nor, Can they talk? but, Can they suffer?" For me, this is what
matters.

## Philosophically, does this experiment raise any new questions?

In general, no. As I've said, there were already a number of reasons, arising
from our extant theories, not to perform an experiment like this. But it's true
that this affair pushes us to keep up with the science. For the moment, there's
no question of creating true moral agents, but should this become the case
(whether they come from AI or from biotechnology), that could lead us toward a
new world, and we need to be ready.
