---
title: L'éthique de la recherche et les cerveaux des singes
---

# L'éthique de la recherche et les cerveaux des singes

J'ai été interviewé par le journal bruxellois _La Libre_ sur l'éthique de la recherche dans le contexte de quelques expériences récentes chez les macaques, qui impliqueraient l'addition d'un gêne responsable pour le développement du cerveau humain dans quelques singes.

Comme toujours avec un interview, le résultat n'est jamais exactement ce dont
j'avais envie de dire, mais je suis plutôt content avec l'article.
(Particulièrement, je crois que l'auteur a voulu le voie
"anti-expérience-animale" de moi, en tant que philosophe, et donc quelques
commentaires sur les expériences _sur le singe_ sont devenus commentaires sur
des expériences _animales._) L'article finale [est disponible
ici,](http://archive.charlespence.net/media/2019-la-libre-singes.pdf) et les
remarques que j'ai envoyées sont ci-dessous.

---

## Floute-t-elle la frontière entre l'homme et l'animal ?

Puisque le but des scientifiques concernés est tout simplement de produire des
singes avec un cerveau entre celui du singe et celui de l'homme, c'est difficile
de dire tout sauf oui. Mais selon moi, bien qu'il soit important de reconnaître
les capacités particulières des humains, ce n'est pas nécessaire de croire que
les singes ainsi produits sont en quelque sorte « humains » pour être
complètement horrifié par cette expérience.

Par exemple, l'un des auteurs de la nouvelle étude a déclaré que, car les sujets
ne sont que des singes (et non des grandes singes), il existe une différence
suffisante pour prétendre que la recherche ne pose pas des problèmes
éthiques. Je ne connais aucune preuve que puisse justifier tellement d'écart au
niveau éthique entre deux clades si proches évolutivement. Au contraire, nous
apprenons peu à peu que, chez les singes et grandes singes (et aussi chez les
céphalopodes, au moins), il y a une vie intérieure riche, et ils sont presque
certainement capables de joie, de souffrance, et de douleur. Quant à moi, ça
suffit pour établir des problèmes éthiques, même s'il n'y a pas d'inquiétude
d'une « Planète des singes. »

## Voyez-vous un intérêt à cette expérience ?

Je trouve qu'il y a deux niveaux d'intérêts à cet égard. D'abord, il y a des
problèmes éthiques liées à n'importe quelle expérience avec des singes comme
sujets. Il est probable que dans certains domaines (p. ex. la transmission du
VIH), l'utilisation du singe comme modèle animal présente des réels
avantages. Mais il faut toujours évaluer s'il n'y a vraiment aucune alternative,
et si les connaissances acquises en valent le mal aux animaux.

Cette expérience cible donc d'ajouter un gène humain aux singes qui contrôle, au
moins en partie, le développement du cerveau. (Ils ont l'intention d'ajouter des
autres gènes dans les mois qui viennent.) La question scientifique abordée est
d'une grande importance : quelles sont les étapes de l'évolution du cerveau
humain ? Pourquoi sommes-nous si distinctifs ? C'est un sujet absolument central
de l'évolution humaine.

Mais comment cette expérience pourrait-elle vraiment l'aborder ? On a des singes
(macaques, plus précisément), qui ont divergé de notre dernier ancêtre commun il
y a environ 23 millions d'années. Évolutivement ils ne étaient pas statiques –
ils ont tout à fait changé beaucoup pendant cette période de temps, et il en est
de même pour les êtres humains (et notre autre ancêtres communs, comme les
gorilles, les chimpanzés, etc., qui sont tous évolués en même temps). Puis,
après 23 millions d'années d'évolution, on va introduire un seul gène humain
dans un singe, et on trouve (peut-être – il faut dire que l'article ne pourrait
pas être publiée dans les grandes revues internationales) qu'ils deviennent un
peu plus accomplis à un examen de mémoire à court terme. (La moitié meurt.)

Ainsi, il n'est pas du tout clair, même en l'absence de soucis éthiques, si
l'expérience éclairera l'évolution humaine. Étant donné les grands problèmes
éthiques, il est inimaginable que l'analyse coûts-bénéfices puisse finalement
être positif.

## Sur quoi se fonde la dignité humaine ou animale. Est-ce justement sur la capacité cognitive de ces espèces ?

J'ai déjà parlé sur la capacité émotionnelle, de la vie riche des animaux. Il ne
faut pas être une véritable utilitariste de partager l'opinion du fondateur du
mouvement, Jeremy Bentham, qui écrit: « La question n'est pas, peuvent-ils
raisonner ? ni, peuvent-ils parler ? mais, peuvent-ils souffrir ? » À mes yeux,
ceci est l'important.

## Philosophiquement, cette expérience aborde-t-elle de nouvelles questions?

Dans l'ensemble, non. Comme je l'ai dit, il y avait déjà de nombreuses raisons,
issues de nos théories existantes, de ne pas réaliser une expérience comme
celle-ci. Mais c'est vrai que cette affaire nous incite à nous tenir au courant
de la science. Pour le moment, il ne s'agit pas de produire des véritables
agents moraux, mais si c'est le cas (qu'elles viennent de l'IA ou de la
biotechnologie), ça pourrait nous mener vers un nouveau monde, et nous devrons
être prêts.
