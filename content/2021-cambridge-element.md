---
title: The Causal Structure of Natural Selection
---

# The Causal Structure of Natural Selection

If you're interested by my contribution to the Cambridge Elements series, here
is a brief introduction to the text. The Elements series, edited (in the
philosophy of biology) by Michael Ruse and Grant Ramsey, is designed to be a set
of what one might call "opinionated introductions" to an area of contemporary
research.

In my case, I've started from a discussion -- at the very least around forty
years old, and, as I've actually argued elsewhere, as old as our discussions of
statistical theorizing about natural selection -- about the question of
causation in natural selection. Natural selection is an interesting sort of
theory. We have properties of individuals, of the kind that Darwin was so
interested in: being stronger, being faster, and so on. Those individuals
interact with one another, and form populations. Certain kinds of events that
can happen to those individuals -- certain ways in which they can systematically
live, mate, die, and so on -- lead to reproducible and explicable kinds of
change at the population level, like the generation of adaptations.

So: how should we understand the general structure of all of that?

In the contemporary philosophy of biology literature, a whole host of questions
have been picked up that roughly cluster around these issues, and unfortunately
have tended to be discussed simultaneously. What are the correct definitions of
concepts like natural selection and genetic drift to be drawn from biological
practice? Are they processes, or population-level outcomes? If they're
processes, are they causal? How should we define fitness? Is it a property of
individual organisms, or of traits? Can it play a causal role, or not? Where is
the "causal action" within an evolving population? At the individual level, or
the population level, or the genetic level (or all or none of the above)? How do
evolutionary explanations work? What is the role of the subjective interests of
observers in the creation of those explanations? How should we understand the
relationship between individuals and populations, and the sort of "multi-level"
system that they produce? What role do mathematical models play in evolutionary
inferences?[^more]

[^more]:
    And more than this, besides, which I don't have the space to get into in a
    short introduction like this one!

As you can see, this is a pretty bewildering variety of issues. For better or
worse, since the early 2000's, they've tended to be grouped together into two
major "positions," which have come to be known as the "causalist" and
"statisticalist" views. For the causalist, evolutionary factors like selection
and drift are causal processes that act upon populations (and, for some
causalists, have analogues that work on individuals, too). The causal action is
therefore at both the level of individuals (mating, reproducing, and dying) and
the level of populations (natural selection, genetic drift, migration, and so
on). Evolutionary explanations are perhaps particularly complex, but
fundamentally aren't unlike other kinds of causal explanations in other special
sciences.[^cists]

[^cists]:
    For paradigmatic causalist papers, see, for instance, Hodge, M. J. S. 1987.
    “Natural Selection as a Causal, Empirical, and Probabilistic Theory.” In
    _The Probabilistic Revolution, Volume 2: Ideas in the Sciences,_ edited by
    Lorenz Krüger, Gerd Gigerenzer, and Mary S. Morgan, 233–70. Cambridge, MA:
    Bradford Books; Millstein, Roberta L. 2006. “Natural Selection as a
    Population-Level Causal Process.” _British Journal for the Philosophy of
    Science_ 57 (4): 627–53. <https://doi.org/10.1093/bjps/axl025>; Pence,
    Charles H., and Grant Ramsey. 2013. “A New Foundation for the Propensity
    Interpretation of Fitness.” _British Journal for the Philosophy of Science_
    64 (4): 851–81. <https://doi.org/10.1093/bjps/axs037>.

For the statisticalists, on the other hand, natural selection and genetic drift
are not causes; they are non-causal statistical abstractions away from the real
causal action at the level of individual organisms. Fitness is nothing more than
a growth rate in a certain kind of population, and evolutionary explanations are
crucially constructed around the subjective decisions of individual researchers
to abstract away from certain kinds of details about organisms.[^sists]

[^sists]:
    For paradigmatic statisticalist papers, see, for instance, Walsh, Denis M.,
    Tim Lewens, and André Ariew. 2002. “The Trials of Life: Natural Selection
    and Random Drift.” _Philosophy of Science_ 69 (3): 429–46.
    <https://doi.org/10.1086/342454>; Matthen, Mohan, and André Ariew. 2002.
    “Two Ways of Thinking about Fitness and Natural Selection.” _Journal of
    Philosophy_ 99 (2): 55–83; Walsh, Denis M., André Ariew, and Mohan
    Matthen. 2017. “Four Pillars of Statisticalism.” _Philosophy, Theory, and
    Practice in Biology_ 9: 1. <https://doi.org/10.3998/ptb.6959004.0009.001>.

I think it's fair to say that this debate has somewhat stagnated in the last few
years. Both causalists and statisticalists continue to publish (and I say this
as someone who has published a number of papers broadly in the causalist vein),
but the arguments have a persistent sense of not engaging with one another.
There's also, I think, a sense that both these positions are more complex and
interesting than they seem when they're simply described as being "causalist" or
"statisticalist" -- or, put differently, there's many different ways that one
might be a causalist or a statisticalist, and to describe these works as _only_
coming from one tradition or the other seems to elide over interesting and
philosophically rich differences of opinion.

I've tried in this work to find a new way to "un-stick" the debate, in two
different ways. First, I've worked to extract a particular subset of these
questions, surrounding causal structures -- which I think are both more
complicated than people often admit, and haven't been very clearly discussed in
isolation so far in the literature. And second, having extracted these
questions, I think, gives us a way to find connections between this question of
causal structure in the biological sciences and other discussions of causal
structure in places like the philosophy of physics and the philosophy of mind. I
make an attempt to draw some preliminary such connections near the end of the
book.

If that's the kind of thing that sounds interesting to you, watch this space!
The book is quite short, and, if Cambridge University Press continues the same
tradition that they have with other books in the series, it should be available
as a free PDF download for the first few weeks after they finish typesetting it.
Keep an eye on my various social media channels and I'll distribute the link as
soon as I have it.[^thx]

[^thx]:
    I'd be remiss not to signal my many thanks to a whole host of people who
    have provided fantastic comments and revisions over the course of the
    process, especially Marshall Abrams (whose own forthcoming book on similar
    subjects, with a slant toward questions in the interpretation of
    probability, will be a must-read), and to detailed draft comments from Brian
    McLoone, Victor Luque, and an anonymous reviewer.
