---
title: Mammoths, Climate Change, and a Long-Vanished Ecosystem
---

# Mammoths, Climate Change, and a Long-Vanished Ecosystem

The local magazine for science students at UCLouvain, _Cubix,_ recently asked me
to contribute a piece on new efforts to resurrect the mammoth as a way to
counteract climate change, pushed most strongly by George Church's group. Here
it is, in a slapdash translation.

---

The climate crisis is now a part of our daily reality, whether we like it or
not. No solution will be easy to implement, and it's no longer possible to
dismiss a proposal as "too fantastic." In one sense, the approach of George
Church, geneticist and one of the founders of synthetic biology, is quite far
from it. Indeed, it's the sort of solution that you might hear from a child:
let's see what the Earth was like the last time the planet was colder, and make
it like that today.

But in this case, we're talking about the reconstruction of an ecosystem that's
today almost disappeared: the "mammoth steppe," once one of the most common
biomes in Europe and Asia. The existence of these massive herbivores kept the
plains free of trees and cold, and also left undisturbed the permafrost, one of
the largest sources of carbon in Eurasia. It's not surprising to see, then, that
reconstructing the mammoth step means reconstructing its key species, the
mammoth. Much less clear, all of a sudden, that the idea of "winding back the
clock" is as simple as it seemed.

Technically, it doesn't seem impossible, even if it was first proposed six years
ago, without many recent technological advances. (A parody of a reconstruction
of a mammoth, an April Fool's joke that was taken seriously in the media, dates
all the way back to 1984!) We start with the new tool CRISPR-Cas9, a kind of
easily reprogrammable genetic scissors that give researchers a powerful tool for
modifying the genome more quickly and cheaply than ever. (This technology won
its inventors the Nobel Prize for 2020 in chemsitry.) Church and his team chose
a dozen genes from the mammoth and implanted them in cells from an Asian
Elephant. Voila, mammoth genes, working once again in real life.

But a long way from a mammoth. These genes, for now, are only expressed in a few
elephant skin cells, and we don't know yet if they would result in the
phenotypes (that is, the observed characters) of a real mammoth, or if these
phenotypes will be those that make it capable to play its role on the steppe.

Even more difficult, even if the genes are well chosen, and one could really
build a mammoth, that doesn't mean we've given birth to an ecosystem. According
to Church, it will take at least 80,000 mammoths to have a real possibility to
recreate the mammoth steppe. And for that, we would outstrip our stock of
elephants, themselves already members of an endangered species. Church already
has plans for artificial uteruses, producing (can we still say "giving birth
to?") thousands of mammoths to populate the steppe.

And all this without having mentioned the ethical and legal aspects. Almost no
law in the world covers the case of the resurrection of a species. What
justifications might be offered? Of course, human curiosity and the progress of
science justify, sometimes by themselves, scientific experiments. But it seems
that the deployment of thousands of artificial uteruses and the reprogramming of
an entire ecosystem require more argument than a simple appeal to curiosity or
knowledge. One can certainly gather lots of scientific results in other areas
(or from just one mammoth!) for the same amount of money, at the very least.

We thus have to balance, on the one hand, the side-effects of a possible
replacement of an entire ecosystem and, on the other hand, the effects of
climate change. One thing to mention straightaway: it's not as though we don't
change or replace ecosystems here and now. The effects of climate change, just
as much caused by use as would be a resurrection of the mammoth steppe, have
already altered (and, of course, threaten to alter much more) the living world.
While the effort of Church remains, for now, closer to science-fiction than to
reality, it's now that we should begin reflecting on it, to avoid a scientfiic,
ethical, legal, and social surprise.
