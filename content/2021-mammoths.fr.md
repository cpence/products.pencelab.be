---
title: Le mammouth, le changement climatique, et un écosystème disparu
---

# Le mammouth, le changement climatique, et un écosystème disparu

Le magazine local pour les étudiants en sciences à l'UCLouvain, _Cubix,_ m'a
récemment demandé de contribuer à un article sur les nouveaux efforts pour
ressusciter le mammouth pour contrer le changement climatique, poussés plus
fortement par le groupe de George Church.

---

La crise climatique est maintenant un constat de notre quotidien, qu’on le
veuille ou non. Aucune solution ne sera facilement implémentée, et il n’est plus
guère admissible de critiquer une proposition en l’appelant un « fantasme ».
Dans un certain sens, l’approche de George Church, généticien et un des
fondateurs de la biologie synthétique, est loin de ça. Effectivement, c’est du
genre qu’on peut entendre d’un enfant : voyons donc à quoi ressemblait la
planète la dernière fois que la Terre était plus froide, et faisons-le comme ça
aujourd’hui.

Mais dans ce cas, il s’agit de la reconstruction d’un écosystème qui est
aujourd’hui presque disparu : « la steppe à mammouths », autrefois un des plus
communs biomes de l’Europe et l’Asie. L’existence de ces grandes herbivores a
gardé les plaines libres d’arbres et froides, et a aussi laissé tranquille le
permafrost, une des plus grandes sources de carbone de l’Eurasie. C’est pas
étonnant de voir, ensuite, que reconstruire la steppe à mammouths veut dire
reconstruire son espèce clé, le mammouth. Beaucoup moins clair, soudain, que
l’idée de « remonter le temps » soit une idée aussi simple qu’elle n’apparaît.

Techniquement, la proposition ne semble pas impossible, bien qu’elle ait été
lancée déjà il y a 6 ans, sans beaucoup de vraie avancée technique récente. (Une
parodie d’une reconstruction du mammouth, poisson d’avril qui a été pris au
sérieux par les médias, date déjà de 1984 !) On commence avec la nouvelle
technique CRISPR-Cas9, une sorte de ciseaux génétique facilement programmable
qui donne une capacité puissante aux chercheurs et chercheuses de modifier le
génome plus rapidement et moins cher que jamais. (Cette technologie a gagné ses
inventeur·e·s le Prix Nobel 2020 en chimie.) Church et son équipe ont choisi une
dizaine de gènes du mammouth et les implantés dans des cellules d’un éléphant
d’Asie. Voilà, des gènes de mammouth, opérant encore dans le vivant.

Mais loin d’un mammouth. Ces gènes, pour l’instant, ne sont exprimés que dans
quelques cellules de peau d’un éléphant, et on ne sait pas encore s’ils
entraînent les phénotypes (c’est-à-dire, les caractéristiques visibles) d’un
vrai mammouth, ou si ces phénotypes seront celles qui lui rendait capable de
jouer son rôle sur la steppe.

Plus difficile encore, même si les gènes sont bien choisis, et on peut bel et
bien façonner un mammouth, ça n’équivaut pas à donner naissance à un écosystème.
Au moins, selon Church, il faut 80 000 mammouths pour avoir une vraie
possibilité de recréer la steppe à mammouths. Et pour ça, on dépasse notre stock
d’éléphants, déjà eux-mêmes membres d’une espèce en voie de disparition. Church
a déjà en vue des utérus artificieux, produisant (peut-on toujours dire
« donnant naissance à » ?) des milliers de mammouths pour peupler la steppe.

Et tout ça sans avoir mentionné les aspects éthiques et légales. Presque aucun
projet de loi dans le monde ne comprend le cas de la résurrection d’une espèce.
Quelles justifications peuvent être offertes ? Bien sûr, la curiosité humaine et
le progrès de la science justifient, parfois seuls, des expériences
scientifiques. Mais il semble que le déploiement de milliers des utérus
artificieux et la reprogrammation de tout un écosystème exigent plus d’argument
qu’un simple appel à la curiosité ou la connaissance. On peut certainement
récolter aussi de résultats scientifiques dans d’autres domaines (ou d’un seul
mammouth !) pour le même montant d’argent, au moins.

Il faut donc mettre dans la balance, d’un côté, les effets secondaires d’un
éventuel remplacement d’un écosystème entier, et de l’autre, les effets du
changement climatique. Une chose à remarquer tout d’abord : c’est pas comme nous
ne changeons ni remplaçons des écosystèmes ici et maintenant. Les effets du
changement climatique, tout autant grâce à nous qu’une résurrection de la steppe
à mammouths serait, ont déjà changé (et, bien sûr, menacent de changer beaucoup
plus) le monde vivant. Lorsque l’effort de Church reste, pour l’instant, plus
proche à la science-fiction qu’à la réalité, c’est maintenant quand on devrait
commencer nos réflexions, afin d’éviter une surprise scientifique, éthique,
légale, et sociale.
