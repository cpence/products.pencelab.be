---
title: Research Products
translationKey: products
---

# Research Products

Our group creates a wide variety of research products, including academic books
and papers, and materials for public audiences. We also give talks to groups of
specialists and the public, worldwide. Check it all out below. For each
publication, members of the lab are marked **in bold.**

Note that as a group, we discourage the posting of our work on for-profit sites
like Academia.edu or ResearchGate. All our articles can instead both be found
here and at a variety of other open-access preprint archives.

## Publications

### Books

{{<publist "book">}}

### Articles and Book Chapters

{{<publist "article" "chapter">}}

### Gray Literature

{{<publist "gray">}}

### Book Reviews

{{<publist "review">}}

### Preprints / Other

{{<publist "other">}}

## Talks

### Upcoming Engagements

The lab is always interested in talking about our work to any audiences who
would like to hear about it. [Send Prof. Pence an
email](mailto:charles@charlespence.net) and we’d love to set something up.

### Recent Talks

{{<talklist 5>}}

A complete archive of older talks [can be found here.]({{<relref "talk-archive">}})

## Outreach

A complete archive of outreach and media activities [can be found here.]({{<
relref "outreach" >}})
