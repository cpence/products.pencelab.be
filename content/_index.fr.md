---
title: Produits de la recherche
translationKey: products
---

# Produits de la recherche

Notre groupe rédige des articles et des livres académiques et des documents
accessibles au grand public. Nous donnons aussi des conférences à des
spécialistes et des non-spécialistes, dans le monde entier. Ci-dessous, veuillez
trouver une liste de nos travaux. Pour chaque publication, membres du
laboratoire sont marqué·e·s **en gras.**

Notez qu'en tant que groupe, nous décourageons la publication de nos travaux sur
des sites à but lucratif comme Academia.edu ou ResearchGate. Tous nos articles
peuvent au contraire être trouvés ici et dans diverses autres archives de
preprints en libre accès.

## Publications

### Livres

{{<publist "book">}}

### Articles et chapitres

{{<publist "article" "chapter">}}

### Littérature grise

{{<publist "gray">}}

### Critiques de livre

{{<publist "review">}}

### Preprints / Autres

{{<publist "other">}}

## Conférences

### Prochains engagements

Le laboratoire et toujours intéressé pour présenter notre recherche à quiconque
veut l’écouter. N’hésitez pas à [envoyer un courriel à Pr.
Pence](mailto:charles@charlespence.net) et on essaiera d’arranger quelque chose.

### Conférences récentes

{{<talklist 5>}}

Une archive complète des conférences [se trouve ici.]({{< relref "talk-archive" >}})

## Service

Une archive complète de notre service à la société et nos interventions dans les
médias [se trouve ici.]({{< relref "outreach" >}})
