---
title: Outreach and Media
---

# Outreach and Media Archive

- [_Futurum
  Careers,_](https://futurumcareers.com/a-crisis-of-clarity-can-defining-biodiversity-help-us-protect-the-natural-world)
  a magazine devoted to popularization and promoting careers in research,
  profiled Pr. Pence
- Prof. Pence was interviewed by the newsletter of the SPSP (Society for the
  Philosophy of Science in Practice), for the summer 2022 issue (#22), [on the
  use of digital humanities in philosophy][spsp]
- Prof. Pence was interviewed by the RTBF (the francophone national radio of
  Belgium) [on the experience of being an American researcher in Belgium (in
  French)][rtbf-remote] ([local copy][rtbf-local])
- Prof. Pence contributed an article to _Cubix_ (the magazine for/by science
  students at UCLouvain) on mammoths and climate change ([PDF,
  French][cubix-pdf] | [local copy]({{<relref "2021-mammoths.md">}})
- Prof. Pence was interviewed by _fnrs.news_ (the newsletter of the Wallonian
  scientific funding agency) on the lab's work in digital humanities ([PDF,
  French][fnrs-pdf] | [local copy][fnrs-local])
- Prof. Pence was interviewed by _La Libre_ (a French-language Brussels
  newspaper) [on recent genetic experiments in monkeys
  (paywall).][lalibre-remote] ([local copy][lalibre-pdf] | [original remarks,
  translated]({{<relref "2019-monkey-genes.md">}}))
- Prof. Pence was interviewed by Jackson Wheat for his channel [on
  Youtube.][wheat-youtube]
- Prof. Pence was interviewed by the East Texas ABC station, KTRE, [about my
  Darwin Day talk at Stephen F. Austin State.][darwin-day-remote] ([local
  copy][darwin-day-local] \| [video download (copyright © Raycom
  Media/KTRE)][darwin-day-vid-dl] \| [video stream (copyright © Raycom
  Media/KTRE)][darwin-day-vid-local])
- Prof. Pence was interviewed and quoted extensively for the _Legacy Magazine_
  article [“The Bright Ages,” in the Fall 2015 issue,][lsu-mag-remote] on the
  ethics of technology ([local copy][lsu-mag-local])
- Prof. Pence’s course on Ethics of New Weapons Technologies at LSU was profiled
  in the [_LSU Reveille_ for November 12, 2014][lsu-paper-remote] ([local
  copy][lsu-paper-local])
- Our work with Grant Ramsey on the evoText project was profiled in [the October
  2014 issue of _NDWorks,_][ndworks] the Notre Dame faculty/staff newsletter
- Prof. Pence’s course on the Ethics of Emerging Weapons Technologies, co-taught
  with Maj. Gen. Robert Latiff, was profiled in [_The New York Times_ for
  February 8, 2014][nyt-remote] ([local copy, print edition
  (20M)][nyt-local-print] \| [local copy, web edition][nyt-local-web])
- The same course was featured in Notre Dame’s “What Would You Fight For?”
  advertising campaign, in the advertisement [“Fighting for the Ethical Use of
  Technology”][tv-ad-remote]
- My talk at the American Philosophical Association’s Eastern Division Meeting
  on genetic drift was covered by [the blog of _Philosophy and Theory in
  Biology_][drift-blog]

[spsp]: https://archive.charlespence.net/media/2022-spsp.pdf
[rtbf-remote]: https://www.rtbf.be/auvio/detail_les-belges-du-bout-du-monde?id=2866808&fbclid=IwAR02YVROuQ3Anc7zuzGKtzVJKoWRUQ1swt90SCayD0CG74kE2dzTRkyRmN0
[rtbf-local]: https://archive.charlespence.net/media/2022-rtbf.mp3
[cubix-pdf]: https://archive.charlespence.net/media/2021-cubix.pdf
[fnrs-pdf]: https://archive.charlespence.net/media/2021-fnrs-dh-fr.pdf
[fnrs-local]: https://pencelab.be/projects/2021-fnrs-news-dh/
[lalibre-remote]: https://www.lalibre.be/debats/ripostes/peut-on-developper-un-cerveau-humain-chez-les-animaux-5cc73eebd8ad586a5ad6e7d7
[lalibre-pdf]: https://archive.charlespence.net/media/2019-la-libre-singes.pdf
[wheat-youtube]: https://www.youtube.com/watch?v=Uqucvy8P0F4
[darwin-day-remote]: http://www.ktre.com/story/34479356/lsu-professor-discusses-ongoing-evolution-vs-creationism-debate-at-sfa
[darwin-day-local]: https://archive.charlespence.net/media/darwin-day-2017.pdf
[darwin-day-vid-dl]: https://archive.charlespence.net/media/darwin-day-2017.mp4
[darwin-day-vid-local]: https://videos.thepences.org/media/darwin-day-2017.html
[lsu-mag-remote]: http://legacymaglsu.tumblr.com/post/134278487673/the-bright-ages
[lsu-mag-local]: https://archive.charlespence.net/media/legacy-2015.pdf
[lsu-paper-remote]: http://www.lsureveille.com/new-course-challenges-ethics-of-war-weapon-technology/article_3c10a61a-6ae3-11e4-bcab-4bd69b81dfaf.html
[lsu-paper-local]: https://archive.charlespence.net/media/lsu-eewt-2014.pdf
[ndworks]: https://archive.charlespence.net/media/ndworks-2014.pdf
[nyt-remote]: http://www.nytimes.com/2014/02/08/us/a-general-in-a-classroom-takes-on-the-ethics-of-war.html
[nyt-local-print]: https://archive.charlespence.net/media/nyt-eewt-2014-print.pdf
[nyt-local-web]: https://archive.charlespence.net/media/nyt-eewt-2014-web.png
[tv-ad-remote]: https://www.youtube.com/watch?v=ZoOpsCQ_u9E
[drift-blog]: http://philosophyandtheoryinbiology.blogspot.com/2012/12/the-philosophy-of-genetic-drift.html
