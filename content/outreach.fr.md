---
title: Service et média
---

# Archive de service et média

- [Le magazine _Futurum
  Careers_](https://futurumcareers.com/a-crisis-of-clarity-can-defining-biodiversity-help-us-protect-the-natural-world)
  a fait le profil de Pr. Pence pour la vulgarisation et la promotion des
  carrières en recherche au niveau secondaire
- Pr. Pence a été interviewé par le newsletter du SPSP (la Société de la
  philosophie de la science en pratique), pour le numéro d'été 2022 (#22), [sur
  l'utilisation des humanités numériques en philosophie (en anglais)][spsp]
- Pr. Pence a été interviewé par la RTBF (la radio nationale wallon) [sur
  l'expérience d'être un chercheur américain en Belgique][rtbf-remote] ([copie
  locale][rtbf-local])
- Pr. Pence a contribué un article au magazine _Cubix_ (de/pour les étudiant·e·s
  en sciences à l'UCLouvain) sur le mammouth et le changement climatique ([PDF,
  français][cubix-pdf] | [copie locale]({{<relref "2021-mammoths.md">}})
- Pr. Pence a été interviewé par _fnrs.news_ (la newsletter de l'agence de
  financement scientifique wallon, le FNRS) sur nos projets dans les humanités
  numériques ([PDF, français][fnrs-pdf] | [copie locale][fnrs-local])
- Pr. Pence a été interviewé par _La Libre_ (un journal français de Bruxelles)
  [sur des récentes expériences génétiques chez les singes
  (paywall).][lalibre-remote] ([copie locale][lalibre-pdf] | [commentaires
  originaux]({{<relref "2019-monkey-genes.md">}}))
- Pr. Pence a été interviewé par Jackson Wheat pour [sa chaîne
  Youtube.][wheat-youtube]
- Pr. Pence a été interviewé par la chaîne ABC au Texas de l’Est, KTRE, [sur ma
  conférence de Darwin Day à Stephen F. Austin State.][darwin-day-remote]
  ([copie locale][darwin-day-local] \| [télécharger vidéo (copyright © Raycom
  Media/KTRE)][darwin-day-vid-dl] \| [transférer vidéo (copyright © Raycom
  Media/KTRE)][darwin-day-vid-local])
- Pr. Pence a été interviewé et cité considérablement pour un article de _Legacy
  Magazine_ [“The Bright Ages,” dans le numéro Fall 2015,][lsu-mag-remote] sur
  l’éthique de la technologie ([copie locale][lsu-mag-local])
- Le cours de Pr. Pence sur l’Éthique des technologies d’armes nouvelles à LSU
  est décrit dans le [_LSU Reveille_ de November 12, 2014][lsu-paper-remote]
  ([copie locale][lsu-paper-local])
- Notre travail avec Grant Ramsey sur le projet evoText est décrit dans [le
  numéro d’octobre 2014 de _NDWorks,_][ndworks] la newsletter de la faculté de
  Notre Dame
- Le cours de Pr. Pence sur l’Éthique des technologies d’armes nouvelles,
  cotitulaire avec Maj. Gen. Robert Latiff, est décrit [dans _The New York
  Times_ de February 8, 2014][nyt-remote] ([copie locale, édition papier
  (20M)][nyt-local-print] \| [copie locale, édition web][nyt-local-web])
- Le même cours est décrit dans la campagne de publicité de Notre Dame “What
  Would You Fight For?”, à la pub [“Fighting for the Ethical Use of
  Technology”][tv-ad-remote]
- La présentation de Pr. Pence à la conférence de la American Philosophical
  Association’s Eastern Division sur la dérive génétique est couverte par [le
  blog de _Philosophy and Theory in Biology_][drift-blog]

[spsp]: https://archive.charlespence.net/media/2022-spsp.pdf
[rtbf-remote]: https://www.rtbf.be/auvio/detail_les-belges-du-bout-du-monde?id=2866808&fbclid=IwAR02YVROuQ3Anc7zuzGKtzVJKoWRUQ1swt90SCayD0CG74kE2dzTRkyRmN0
[rtbf-local]: https://archive.charlespence.net/media/2022-rtbf.mp3
[cubix-pdf]: https://archive.charlespence.net/media/2021-cubix.pdf
[fnrs-pdf]: https://archive.charlespence.net/media/2021-fnrs-dh-fr.pdf
[fnrs-local]: https://pencelab.be/fr/projets/2021-fnrs-news-dh/
[lalibre-remote]: https://www.lalibre.be/debats/ripostes/peut-on-developper-un-cerveau-humain-chez-les-animaux-5cc73eebd8ad586a5ad6e7d7
[lalibre-pdf]: https://archive.charlespence.net/media/2019-la-libre-singes.pdf
[wheat-youtube]: https://www.youtube.com/watch?v=Uqucvy8P0F4
[darwin-day-remote]: http://www.ktre.com/story/34479356/lsu-professor-discusses-ongoing-evolution-vs-creationism-debate-at-sfa
[darwin-day-local]: https://archive.charlespence.net/media/darwin-day-2017.pdf
[darwin-day-vid-dl]: https://archive.charlespence.net/media/darwin-day-2017.mp4
[darwin-day-vid-local]: https://videos.thepences.org/media/darwin-day-2017.html
[lsu-mag-remote]: http://legacymaglsu.tumblr.com/post/134278487673/the-bright-ages
[lsu-mag-local]: https://archive.charlespence.net/media/legacy-2015.pdf
[lsu-paper-remote]: http://www.lsureveille.com/new-course-challenges-ethics-of-war-weapon-technology/article_3c10a61a-6ae3-11e4-bcab-4bd69b81dfaf.html
[lsu-paper-local]: https://archive.charlespence.net/media/lsu-eewt-2014.pdf
[ndworks]: https://archive.charlespence.net/media/ndworks-2014.pdf
[nyt-remote]: http://www.nytimes.com/2014/02/08/us/a-general-in-a-classroom-takes-on-the-ethics-of-war.html
[nyt-local-print]: https://archive.charlespence.net/media/nyt-eewt-2014-print.pdf
[nyt-local-web]: https://archive.charlespence.net/media/nyt-eewt-2014-web.png
[tv-ad-remote]: https://www.youtube.com/watch?v=ZoOpsCQ_u9E
[drift-blog]: http://philosophyandtheoryinbiology.blogspot.com/2012/12/the-philosophy-of-genetic-drift.html
